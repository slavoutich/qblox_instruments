.. include:: ../README.rst

########
Contents
########

    .. toctree::
        :maxdepth: 2
        :caption: Getting started
        :name: getting_started

        getting_started/overview.rst
        getting_started/installation.rst
        getting_started/connecting_pulsar.rst
        getting_started/connecting_spi_rack.rst
        getting_started/updating.rst
        getting_started/what_is_next.rst

    .. toctree::
        :maxdepth: 2
        :caption: Documentation
        :name: documentation

        documentation/general.rst
        documentation/pulsar.rst
        documentation/sequencer.rst
        documentation/synchronization.rst
        documentation/troubleshooting.rst

    .. toctree::
        :maxdepth: 2
        :caption: Tutorials
        :name: tutorials

        tutorials/cont_wave_mode.ipynb
        tutorials/basic_sequencing.ipynb
        tutorials/advanced_sequencing.ipynb
        tutorials/mixer_correction.ipynb
        tutorials/scope_acquisition.ipynb
        tutorials/binned_acquisition.ipynb
        tutorials/synchronization.ipynb
        tutorials/multiplexed_sequencing.ipynb
        tutorials/rf_control.ipynb
        tutorials/rabi_experiment.ipynb
        tutorials/spi_rack.ipynb

    .. toctree::
        :maxdepth: 2
        :caption: API reference
        :name: api_reference

        api_reference/pulsar_qcm.rst
        api_reference/pulsar_qrm.rst
        api_reference/cluster.rst
        api_reference/spi_rack.rst
        api_reference/ieee488_2.rst

##################
Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
